# Consulting Associate-2021-Online-Coding-Assessment

Please read through below instructions before starting the assessment. You will be given **2-hour** for completing the assessment.


## 1. Pre-requisite
- NPM version 6.14.4 above (Package Manager)
- Node JS version 10.19.0 above (Runtime Environment)

## 2. Setup
### 2.1 Before Start
Download the source code in this repository and run below command under the root directory to install the dependency.

```
npm install
```

### 2.2 Backend Application

server.js is the entry point the backend application, other backend related coding logic is placed under folder `server/`

Start the backend application with the command below.
```
node server.js
```

### 2.3 Frontend Application

All the frontend coding logic is placed under the folder `src/`

Start the frontend application in development mode with the command below
```
npm run start
```
Goto http://localhost:3000/ through web browser, you should see a store website now.

### 2.4 Database

A SQLite file-based database named `shop.db` is placed under `server/db/` folder. You are expected to use the provided client based on your OS (Window 7/10, MacOS) to connect to the database by executing below command

Window
```
server/db/sqlite3-window
```

MacOS
```
server/db/sqlite3-macos
```

To open the provided database in the CLI
```
> .open shop.db
```

Tips:
| command | description |
| ------ | ------ |
| .table | List the existing table in databse after connected |
| .schema {Table} | Look up the table schema |

Note that you may need to search other necessary commands for the assessment.

## 3. Assessment

Imagine that you are a technology consultant, and you are helping on a demo of an e-commerce B2C online website project. The first version of the prototype is implemented, but there are some comments given by the project's partner after he/she reviewed your demo and re-examine the client's requirements. Please find the questions as below.

(1) Since the client has recently decided to re-brand its company, please help change the store name from "IBM Shop" to "International Online Mall" (Suggested time: 5 Minutes)

(2) Client would like to perform an one-off clean-up action to remove some obsoleted products on home page, but at the same time keep the obsoleted product data in the product table for future reference. Please help update the database schema, query and mark the product data which fulfill following criteria as **OBSOLETED**. (Suggested time: 25 Minutes)

- A product will be treated as **OBSOLETED** when the `type` is **B** and `price` is higher than **$20** at this moment.

(3) Partner would like you to integrate the membership page with server data through API like product and cart page, the API should support filtering by `grade`. (Suggested time: 30 Minutes)

(4) Your team lead reported an issue on shopping cart page (http://localhost:3000/cart). The behaviour is not expected when adding two exact same products into the shopping cart. Please investigate and fix it. (Suggested time: 45 Minutes)

(5) Short essay: Please provide comments on the UI/UX of this website. Any suggestions to improve its UI/UX? Please submit the answer in the Google form below.


**You are allowed to make any reasonable assumption for the assessment, if any. Please add an 'assumption.txt' text file under root directory of the project if so.**


## 4. Submission
Please commit your revised code into your own source code repository and pass it to IBM recruitment team via below URL
https://docs.google.com/forms/d/e/1FAIpQLSefoNZPVTw2Lpxd22SOO6l83DFOjWcXo5p5vJKvnGysRszhJw/viewform

N.B.1: You are expected to commit your code and submit the URL within the assessment period. Late submission will not be considered.

N.B.2: You are strongly not encourged to have duplicated submission. If so, IBM recruitment team will take the latest submission as the final version.
