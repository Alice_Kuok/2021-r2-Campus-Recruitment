import config from '../../config/config.json';
import axios from 'axios';

function filter_obsoleted(data){
  var filtered_table = []
  for (var i = 0; i < data.length; i++){
    var price = data[i].price
    var type = data[i].type
    if (price >= 20 && type === "B"){
      continue
    }
    else{
      filtered_table.push(data[i])
    }
  }
  return filtered_table
}

export const getProduct = () => {
    return axios.get(config.SERVER_ENDPOINT + '/product')
                .then((res) => {
                    // return filter_obsoleted(res.data.data);
                    return res.data.data;
                }).catch((err) => {
                    console.log("API ERROR");
                });
}
