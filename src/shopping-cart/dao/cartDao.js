import config from '../../config/config.json';
import axios from 'axios';

class current_cart{
    constructor(){
      this.data = {};

    }
}

export const getCart = async() => {
    return axios.get(config.SERVER_ENDPOINT + '/cart')
                .then((res) => {
                    return res.data.data
                }).catch((err) => {
                    console.log("API ERROR");
                });
}

export const addCart = async(id) => {
    return axios.post(config.SERVER_ENDPOINT + `/cart?productId=${id}`)
                .then((res) => {
                    return res.data.data
                }).catch((err) => {
                    console.log("API ERROR");
                });
}

export const deleteCart = async() =>{
    return axios.delete(config.SERVER_ENDPOINT + `/cart`)
                .then((res) => {
                    return res.data.data
                }).catch((err) => {
                    console.log("API ERROR");
                });
}
